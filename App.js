
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React from 'react';
 import {Provider} from 'react-redux';
 import AppNavigator from './src/router/Router';
 import {store, persistor} from './src/redux/store/index';
 import {PersistGate} from 'redux-persist/lib/integration/react';
 
 export default class App extends React.Component {
   constructor(props) {
     super(props);
     this.state = {};
   }
 
   componentDidMount() {
     console.log('[App props]', this.props);
     console.log('[store]', store);
   }
 
   render() {
     let globalValue = '我是全局的';
     return (
       <Provider store={store}>
         <PersistGate loading={null} persistor={persistor}>
           <AppNavigator screenProps={{globalValue}} />
         </PersistGate>
       </Provider>
     );
   }
 }
 