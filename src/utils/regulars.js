/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-08-24 14:13:03
 * @LastEditors: daxiong
 * @LastEditTime: 2021-09-23 10:24:40
 * @FilePath: _customer\src\utils\regulars.js
 */
export const isPhone = phone => {
  return /^1(3|4|5|6|7|8|9)\d{9}$/.test(phone);
};
// 判断是否为中文姓名
export const isChineseName = name => {
  return /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/.test(name);
};

// 数字字母组合
export const replaceIDNumber = t => {
  return t.replace(/[^a-zA-Z0-9]/g, '');
};
// 纯数字
export const pureNumber = t => {
  return t.replace(/[^\d]+/g, '');
};
// 纯数中文
export const pureText = t => {
  return t.replace(/[^\u4e00-\u9fa5]/g, '');
};
// 不能输入表情
export const regEmoji = t => {
  return t.replace(
    /[^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n]/g,
    '',
  );
};

//textInput判断内容是否为空，过滤空格、回车
export const isEmpty = html => {
  return (
    html
      .replace(/\r\n|\n|\r/, '')
      .replace(/(?:^[ \t\n\r]+)|(?:[ \t\n\r]+$)/g, '') == ''
  );
};
//过滤空格
export const filterBlankSpace = (value)=>{
  return value.replace(/\s+/g, "");
}

//验证手机号（宽松，非0开头，只要是13,14,15,16,17,18,19开头即可）
export const regPhoneNumber = /0?(13|14|15|17|18|19)[0-9]{9}/;
//手机号(严谨), 根据工信部2019年最新公布的手机号段
export const regRigorousPhoneNumber = /^1((3[\d])|(4[5,6,7,9])|(5[0-3,5-9])|(6[5-7])|(7[0-8])|(8[\d])|(9[1,8,9]))\d{8}$/;
//匹配文本全是空格
export const regSpaces = /^\s*$/;
//匹配纯数字
export const regPureNumber = /^[0-9]*$/;
//匹配https
export const regHttps = /^https/;
//匹配http或者https开头
export const regHttpOrHttpsFront = /(http|https):\/\/([\w.]+\/?)\S*/;
//0 和正整数
export const regPositiveNumber = /^(0|\+?[1-9][0-9]*)$/;
//正小数
export const regQuantityFloat = /^[1-9]\d*\.\d*|0\.\d*[1-9]\d*$/;
//非负浮点数（正浮点数 + 0）
export const regNoNegativeNumberFloat = /^\d+(\.\d+)?$/;
//浮点数或整数
export const regFloatOrIntegerPrice = /^([+]\d+[.]\d+|[-]\d+[.]\d+|\d+[.]\d+|[+]\d+|[-]\d+|\d+)$/;
//浮点数
export const regFloat = /^-?([1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0)$/;
//字符
export const regReplaceCharacter = /[`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。]/im;
//字符，价目管理，关键字录入 除开[；;]
export const regReplaceKeyword = /[`~!@#$%^&*()_\-+=<>?:"{}|,.\/'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】‘’，。、]/im;
//字符，价目管理，项目名称录入 除开[（）,();《》，。、；：%“”]
export const regReplaceProjectName = /[`~!@#$^&*_\-+=<>?:"{}|.\/'\\[\]·~！@#￥……&*——\-+={}|？【】‘’]/im;
//表情
export const regReplaceExpression = /[^\u0020-\u007E\u00A0-\u00BE\u2E80-\uA4CF\uF900-\uFAFF\uFE30-\uFE4F\uFF00-\uFFEF\u0080-\u009F\u2000-\u201f\u2026\u2022\u20ac\r\n]/g;
//验证中文姓名2-10个字
export const regCheckChineseName = /^[\u4E00-\u9FA5]{2,10}$/;
// 正数、负数、和小数
export const regCheckPND = /^(\-|\+)?\d+(\.\d+)?$/;
