/*
 * @Description 自定义loading Toast
 * @Author: daxiong
 * @Date: 2020-07-20 11:24:52
 * @Last Modified by: daxiong
 * @Last Modified time: 2020-07-22 16:09:00
 */

import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Modal,
  Image,
  ActivityIndicator,
} from 'react-native';
import {
  css,
  border,
  borderB,
  borderT,
  padding,
  paddingLR,
  paddingTB,
  paddingT,
  paddingR,
  paddingB,
  paddingL,
  marginLR,
  marginTB,
  marginT,
  marginR,
  marginB,
  marginL,
  fontSize,
  bdRadius,
} from '../../utils/BaseStyle';
import {scaleSize} from '../../utils/ScreenUtils';

export default class LoadingToast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false, //LoadingToast是否显示，默认不显示
      mask: true, //是否有遮罩，默认有，（暂时只支持有遮罩）
      content: '正在加载中', //提示内容
      duration: 0, //LoadingToast显示持续时间（即多少秒后消失）,0-不会消失，需主动调用hide才消失；
      iconShow: true, //是否显示loading icon，默认显示，
    };
  }

  static defaultProps = {
    content: '正在加载中', //提示内容
  };

  //组件渲染前调用
  componentWillMount() {}

  hide() {
    this.setState({
      isShow: false,
    });
  }

  show(res) {
    console.log('[LoadingToast] show=>', res);
    const initiativeHide = () => {
      if (res.duration > 0) {
        setTimeout(() => {
          this.hide();
        }, res.duration);
      }
    };
    const _show = () => {
      // if (res.content) {
      //   this.state.content = res.content;
      // } else {
      //   this.state.content = this.props.content;
      // }
      this.state.content = res.content;
      if (typeof res.iconShow === 'boolean') {
        this.state.iconShow = res.iconShow;
      }
      this.setState(
        {
          isShow: true,
          content: this.state.content,
          iconShow: this.state.iconShow,
        },
        () => {
          initiativeHide();
        },
      );
    };
    if (res) {
      if (typeof res === 'object') {
        if (JSON.stringify(res) === '{}') {
          // console.error('The show method parameter of the LoadingToast component should not be an empty object.');
          _show();
          return;
        }
        if (Array.isArray(res)) {
          console.error(
            'The show method parameter of the LoadingToast component should be an object.',
          );
          return;
        }
        _show();
      } else {
        console.error(
          'The show method parameter of the LoadingToast component should be an object.',
        );
      }
    } else {
      this.setState({
        content: '',
        isShow: true,
      });
    }
  }

  componentWillReceiveProps(res) {
    // console.log('[componentWillReceiveProps]', res);
  }

  render() {
    return this.renderHaveMaskToast();
    // if (mask) {
    //   return this.renderHaveMaskToast();
    // } else {
    //   return this.renderNoMaskToast();
    // }
  }

  //返回有遮罩的Toast
  renderHaveMaskToast() {
    const {isShow} = this.state;
    return (
      <Modal visible={isShow} animationType="none" transparent={true}>
        <View
          style={[
            css.flexRow_cc,
            css.flex1,
            {backgroundColor: 'rgba(0,0,0,0)'},
          ]}>
          <View
            style={[
              styles.dialogWrap,
              css.tc,
              css.flexV,
              bdRadius(8),
              paddingTB(10),
              {
                backgroundColor: 'rgba(0,0,0,.6)',
              },
            ]}>
            {this.renderLoadingIcon()}
            {this.renderTextView()}
          </View>
        </View>
      </Modal>
    );
  }

  renderTextView() {
    const {content} = this.state;
    if (content) {
      return (
        <Text
          numberOfLines={3}
          style={[css.cf, css.tc, paddingLR(10), paddingTB(20), fontSize(28)]}>
          {content}
        </Text>
      );
    } else {
      return <View style={[paddingTB(10)]} />;
    }
  }

  //返回无遮罩的Toast
  renderNoMaskToast() {}

  renderLoadingIcon() {
    const {iconShow} = this.state;
    if (iconShow) {
      return (
        <ActivityIndicator
          size="large"
          color="#FFFFFF"
          style={[paddingT(20)]}
        />
      );
    }
    return null;
  }
}

const styles = StyleSheet.create({
  dialogWrap: {
    minWidth: scaleSize(260),
    maxWidth: scaleSize(420),
  },
});
