/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2021-11-03 19:00:19
 * @LastEditors: daxiong
 * @LastEditTime: 2021-11-03 20:00:02
 */

import React from 'react';
import {
    ScrollView,
    RefreshControl,
} from 'react-native';
// import PropTypes from 'prop-types';

class ScrollViewPull extends React.Component {
    static navigationOptions = {
        header: null,
    };

    static propTypes = {
        // style: PropTypes.object, // 样式
        // refreshing: PropTypes.bool.isRequired,//是否开始下拉刷新动画
        // refreshBegin: PropTypes.func,// 开始下拉刷新回调
        // scrollEnd: PropTypes.func,// 触底回调
    };

    constructor(props) {
        super(props);
        this.initState();
        this.state = {
            offsetY: 0
        }
    }


    initState = () => {

    };

    onRefresh = () => {
        this.props.refreshBegin();
    };

    // 监听上拉触底
    _contentViewScroll = (e) => {
        let offsetY = e.nativeEvent.contentOffset.y; //滑动距离
        let contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
        let oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
        console.log('nativeEvent',e.nativeEvent)
        // console.log('_contentViewScroll',offsetY + oriageScrollHeight,'/',contentSizeHeight)
        if (offsetY + oriageScrollHeight + 1 >= contentSizeHeight) {
            this.props.scrollEnd();
        }
        this.state.offsetY = offsetY;
        // if (typeof this.props.onScroll == 'function') {
        //     this.props.onScroll({ y: offsetY })
        // }
    }

    resultScrollValue = () => {
        return { y: this.state.offsetY }
    }

    render() {
        const { children, refreshing, style } = this.props;
        return (
            <ScrollView
                style={[{ flex: 1 }, style]}
                showsVerticalScrollIndicator={false}
                scrollToIndex
                {...this.props}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
                onMomentumScrollEnd={this._contentViewScroll}
            >
                {children}
            </ScrollView>
        );
    }
}

export default ScrollViewPull;