/*
 * @Descripttion: android/ios按钮封装
 * @Author: daxiong
 * @Date: 2021-01-22 17:04:47
 * @LastEditors: daxiong
 * @LastEditTime: 2021-11-03 18:34:19
 */
import React, { Component } from "react";
import { Text, TouchableOpacity } from "react-native";
import { scaleSize } from "../../utils/ScreenUtils";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  static defaultProps = {
    title: '',
    activeOpacity: 1,
    style: {},  //按钮容器样式
    textStyle: { color: '#333333', fontSize: scaleSize(24) },  //按钮文字样式
  }

  render() {
    if (this.props.children && this.props.title) {
      return (
        <TouchableOpacity style={[this.props.style]} activeOpacity={this.props.activeOpacity} {...this.props}>
          <Text style={[this.props.textStyle]}>{this.props.title}</Text>
          {this.props.children}
        </TouchableOpacity>
      )
    }
    if (this.props.children && !this.props.title) {
      return (
        <TouchableOpacity style={[this.props.style]} activeOpacity={this.props.activeOpacity} {...this.props}>
          {this.props.children}
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity style={[this.props.style]} activeOpacity={this.props.activeOpacity} {...this.props}>
        <Text style={[this.props.textStyle]}>{this.props.title}</Text>
      </TouchableOpacity>
    )
  }
}
