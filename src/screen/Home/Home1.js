/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:42:00
 * @LastEditors: daxiong
 * @LastEditTime: 2021-11-03 18:28:08
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\Home1.js
 */
import React, { Component } from 'react';
import { View, Text, Button, ScrollView } from 'react-native';
import { css, paddingT } from '../../utils/BaseStyle';
import { scaleSize } from '../../utils/ScreenUtils';
import TitleBar from '../../component/TitleBar';
import LoadingToast from '../../component/LoadingToast';
import ToastMsg from '../../utils/ToastMsg';
import { connect } from 'react-redux';
import { changeName, updateChatListState } from '../../redux/action';
import UserListView from "./View/UserListView";

class Home1 extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    if (this.params.routes) {
      this.params.routes = [...this.params.routes, ...['Home1']];
    } else {
      this.params.routes = ['Home1'];
    }
    this.state = {};
  }

  componentDidMount() {
    console.log('[Home1 props]=>', this.props);
  }

  goto = () => {
    // this.props.navigation.navigate('My');
    this.props.navigation.push('Home2', this.params);
  };

  backEvent = () => {
    this.props.navigation.goBack();
  };

  changeValue = () => {
    this.props.dispatch(changeName('002', 'Home1修改的值'));
  };

  render() {
    return (
      <View style={[css.flex1]}>
        <TitleBar
          title={'我是标题'}
          navigation={this.props.navigation}
          backEvent={this.backEvent}
        />
        <ScrollView style={[css.flex1]}>
         
        </ScrollView>
      </View>
    );
  }

  renderItemChat() {
    const { item } = this.params;
    const { updateChatListState } = this.props;
    for (let i = 0; i < updateChatListState.length; i++) {
      if (updateChatListState[i].id == item.id) {
        return <UserListView item={updateChatListState[i]} index={999} navigation={this.props.navigation} />;
      }
    }


  }

  renderChatList() {
    const { updateChatListState } = this.props;
    let listView = [];
    updateChatListState.map((item, index) => {
      listView.push(<UserListView key={index} item={item} index={index} navigation={this.props.navigation} />)
    });
    return listView;
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  updateChatListState: state.updateChatListState,
});

export default connect(mapStateToProps)(Home1);
