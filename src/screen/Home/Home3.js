/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:42:00
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-15 16:16:22
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\Home3.js
 */
import React, {Component} from 'react';
import {View, Text, Button, BackHandler} from 'react-native';
import { StackActions, NavigationActions } from "react-navigation";
import ShowModal from '../../component/ShowModal';

export default class Home3 extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    if (this.params.routes) {
      this.params.routes = [...this.params.routes, ...['Home3']];
    } else {
      this.params.routes = ['Home3'];
    }
    this.state = {};
  }

  componentDidMount() {
    console.log('[Home3 params]=>', this.params);
    if (Platform.OS === 'android') {
      // this.backHandler =
      BackHandler.addEventListener(
        'hardwareBackPress',
        this.onBackButtonPressAndroid,
      );
    }
  }

  componentWillUnmount() {
    console.log('组件卸载', this.onBackButtonPressAndroid);
    // this.backHandler && this.backHandler.remove("hardwareBackPress");
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.onBackButtonPressAndroid,
    );
  }

  onBackButtonPressAndroid = () => {
    const routeName = this.props.navigation.state.routeName;
    console.log('routeName=>', routeName);
    if (this.params.routes.indexOf('Home3') >= 0) {
      // if (routeName === "Home3") {
      this.pressLeft();
      return true;
    } else {
      return false;
    }
  };

  //左上角返回
  pressLeft = () => {
    //BookingProjectSettlementPage-购买手术项目；MyBookingsPage-先做后付付款/未付款订单重新付款
    if (this.params.routes.indexOf('Home3') >= 0) {
      this.ShowModal.show({
        content: '订单已经生成，是否要放弃本次支付？',
        btns: ['继续支付', '放弃'],
        success: (res) => {
          if (res.confirm) {
            this.routesJump();
          }
        },
      });
    } else {
      this.props.navigation.goBack();
    }
  };

  routesJump = () => {
    this.props.navigation.reset(
      [NavigationActions.navigate({routeName: 'My1'})],
      0,
    );
    // this.props.navigation.navigate("MyBookingsPage", {
    //   scene: "Pay",
    //   id: 1,
    //   routes: [],
    // });
  };

  goto = () => {
    // this.props.navigation.push('My2');
    this.props.navigation.reset(
      [NavigationActions.navigate({routeName: 'My1'})],
      0,
    );
    // this.props.navigation.popToTop({ceshi:'测试方法'});
    // this.props.navigation.navigate('My2');
  };

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="最后一页" onPress={this.goto} />
        <ShowModal
          ref={(c) => {
            this.ShowModal = c;
          }}
        />
      </View>
    );
  }
}
