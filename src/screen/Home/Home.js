import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView, Button, NativeModules, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
// import VideoPlayer from 'react-native-rn-videoplayer';
import {
  changeName,
  updateLoginStatus,
  updateChatListState
} from '../../redux/action';
import UserListView from "./View/UserListView";
import { scaleSize } from '../../utils/ScreenUtils';
import FilterTabs from "../../component/FilterTabs";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      num: 123,
      refreshing: false,
      tabIndex: 0,
      viewLength: 0
    };
  }

  componentDidMount() {
    let defaultItem = [
      {
        id: 1,
        name: '话题1',
        likeNumber: 0
      }, {
        id: 2,
        name: '话题2',
        likeNumber: 0
      }, {
        id: 3,
        name: '话题3',
        likeNumber: 0
      }, {
        id: 4,
        name: '话题4',
        likeNumber: 0
      }, {
        id: 9999,
        name: '第N话题',
        likeNumber: 0
      }]
    console.log('[Home props]', this.props);
    this.props.dispatch(updateChatListState({ list: defaultItem, command: 'updateList' }))
  }

  goto = () => {
    this.props.navigation.push('Home1', { routes: ['Home'] });
  };

  changeValue = () => {

  };

  loginOut = () => {
    this.props.dispatch(updateLoginStatus('login_no'));
    this.props.navigation.reset(
      [NavigationActions.navigate({ routeName: 'Login' })],
      0,
    );
  };

  sethookValue = () => {
    this.setState({
      num: '新值'
    })
  }

  btnItemEvent(index, likeNumber) {
    console.log('[btnItemEvent]')
  }

  handleBtnEvent = (opts) => {
    const { action } = opts;
    if (action == 'waterfull') {
      this.props.navigation.navigate('SectionWaterfall')
    }
  }

  render() {
    const Tabs = [{ text: '选项是卡' },{ text: '选项卡' },{ text: '选项卡发多少' },{ text: '选项发卡' },{ text: '选项卡' },{ text: '选发项卡' },{ text: '选项官方卡' },{ text: '选项卡' },{ text: '选项卡' }];
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <Button title="Home1跳转" onPress={this.goto} />
          <View style={styles.itemCell}>
            <Button title="修改值" onPress={this.changeValue} />
          </View>
          <View style={styles.itemCell}>
            <Button title="退出登录" onPress={this.loginOut} />
          </View>
          <View style={styles.itemCell}>
            <Button title="瀑布流" onPress={() => this.handleBtnEvent({ action: 'waterfull' })} />
          </View>

          <FilterTabs tabs={Tabs} activeIndex={4} />

          {/* <View style={styles.itemCell}>
            {this.renderChatList()}
            <Text>登录状态：{this.props.loginStatus}</Text>
          </View> */}

        </ScrollView>
      </View>
    );
  }

  renderList() {
    const { user } = this.props;
    let listView = [];
    user.map((item, index) => {
      listView.push(
        <View key={index}>
          <View>
            <Text>姓名：{item.name}            点赞：{item.likeNumber}</Text>
            <Button title="点赞" onPress={() => this.btnItemEvent(index, item.likeNumber)} />
          </View>
        </View>
      );
    });
    return listView;
  }

  renderChatList() {
    const { updateChatListState } = this.props;
    let listView = [];
    updateChatListState.map((item, index) => {
      // listView.push(
      //   <View key={index}>
      //     <View>
      //       <Text>姓名：{item.name}            点赞：{item.likeNumber}</Text>
      //       <Button title="点赞" onPress={() => this.btnItemEvent(index, item.likeNumber)} />
      //     </View>
      //   </View>,
      // );
      listView.push(<UserListView key={index} item={item} index={index} navigation={this.props.navigation} />)
    });
    return listView;
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  loginStatus: state.loginStatus,
  updateChatListState: state.updateChatListState
});

export default connect(mapStateToProps)(Home);

const styles = StyleSheet.create({
  itemCell: {
    marginTop: scaleSize(20)
  },
})
