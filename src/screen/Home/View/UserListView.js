/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2021-02-05 09:55:01
 * @LastEditors: daxiong
 * @LastEditTime: 2021-02-05 11:54:58
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\View\UserListView.js
 */

import React, { useState, useEffect, useReducer } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { css, paddingT, marginT, paddingTB, marginB } from '../../../utils/BaseStyle';
import Button from "../../../component/Button";
import {
    updateChatListState,
} from '../../../redux/action';


function UserListView(props) {
    let { item, index, navigation, dispatch } = props;

    console.log('[UserListView props]', props)

    function btnItemEvent(name) {
        // item.likeNumber++;
        // dispatch(updateChatListState({
        //     item: {
        //         id: 5,
        //         likeNumber: 0,
        //         name: "少侠"
        //     }, command: 'pushItem'
        // }));
        if (name == 'add') {
            dispatch(updateChatListState({ item: item, command: 'addLike' }));
        }
        if (name == 'red') {
            dispatch(updateChatListState({ item: item, command: 'redLike' }));
        }
    }

    function goToDetail() {
        navigation && navigation.navigate('Home1', { routes: ['Home'], item: item });
    }

    return (
        <View key={index}>
            <View style={[marginB(20), { backgroundColor: '#ddd' }]}>
                <Button style={[paddingTB(30), { backgroundColor: '#ddd' }]} onPress={() => goToDetail()}>
                    <Text>姓名：{item.name}            点赞：{item.likeNumber} </Text>
                </Button>
                <Button title="点赞" style={[css.flexVH, paddingTB(20), { backgroundColor: '#999' }]} onPress={()=>btnItemEvent('add')} />
                <Button title="取消点赞" style={[css.flexVH, paddingTB(20),marginT(10), { backgroundColor: '#999' }]}onPress={()=>btnItemEvent('red')} />
            </View>
        </View>
    )
}

const mapStateToProps = (state) => ({
    loginStatus: state.loginStatus
});

export default connect(mapStateToProps)(UserListView);