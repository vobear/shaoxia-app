/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-29 17:02:43
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-29 17:10:34
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\View\HookReducer.js
 */
import React, {useState, useEffect,useReducer} from 'react';
import {View, Text, Button} from 'react-native';

const initialState = {count: 0};

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    default:
      throw new Error();
  }
}

const Counter = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <View>
      <Text>Count: {state.count}</Text>
      <Button
        title="减"
        onPress={() => {
          dispatch({type: 'decrement'});
        }}
      />
      <Button
        title="加"
        onPress={() => {
          dispatch({type: 'increment'});
        }}
      />
    </View>
  );
};

export default Counter;
