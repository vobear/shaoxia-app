/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-29 14:34:58
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-29 16:59:39
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\View\useFirendStatus.js
 */
import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';

function useFriendStatus(userName) {
  const [isOnline, setIsOnline] = useState(null);

  console.log('[useFriendStatus]', userName);

  useEffect(() => {
    function handleStatusChange(status) {
      setIsOnline(status);
    }
    setTimeout(() => {
      if (userName === 'A') {
        handleStatusChange('Online');
      } else {
        handleStatusChange('NOline');
      }
    }, 3000);
    // ChatAPI.subscribeToFriendStatus(friendID, handleStatusChange);
    // return () => {
    //   ChatAPI.unsubscribeFromFriendStatus(friendID, handleStatusChange);
    // };
  });

  return isOnline;
}

export default useFriendStatus;
