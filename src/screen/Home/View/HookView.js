/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-29 10:15:22
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-29 15:05:10
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\Home\View\HookView.js
 */
import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import useFriendStatus from "./UseFirendStatus";

const HookView = (res) => {
  const [count, setCount] = useState(0);
  const [userName, setIsOnline] = useState(null);
  // console.log('[HookView res]=>', res);
  const _isOnline = useFriendStatus(userName);

 console.log('[useFriendStatus]=>', _isOnline);
  useEffect(() => {
    // console.log('[useEffect]=>', count);
    // function handleStatusChange(status) {
    //   setIsOnline(status);
    // }

    setTimeout(() => {
      setIsOnline('A');
    }, 3000);

    // setTimeout(() => {
    //   handleStatusChange('Offline');
    // }, 6000);
  });

  return (
    <View>
      <Text>好友状态：{JSON.stringify(_isOnline)}</Text>
      <Button
        title="Click me"
        onPress={() => {
          setCount(count + 1);
        }}
      />
    </View>
  );
};

export default HookView;
