import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import useFirendStatus from './UseFirendStatus';

const HookViewOne = ({}) => {
  const [count, setCount] = useState(0);
  const [num, setNum] = useState(0);
  const onLine = useFirendStatus();

  // useEffect(() => {
  //   console.log('[useEffect]=>', count);
  // });

  return (
    <View>
      <Text>HookViewOne组件</Text>
      <Text>好友状态：{onLine}</Text>
      {/* <Text>{JSON.stringify(num)}</Text> */}
      {/* <Button
        title="Click me"
        onPress={() => {
          setCount(count + 1);
        }}
      /> */}
    </View>
  );
};

export default HookViewOne;
