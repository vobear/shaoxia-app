/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 11:41:52
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-26 17:04:36
 * @FilePath: e:\rn-dev\react-native-myapp\src\screen\My\My.js
 */
import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {changeName} from '../../redux/action';

class My extends Component {
  goto = () => {
    // this.props.navigation.goBack();
    this.props.navigation.push('My1');
  };

  changeValue = () => {
    this.props.dispatch(changeName('002', '少侠'));
  };

  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Button title="My1跳转" onPress={this.goto} />

        <Button title="修改值" onPress={this.changeValue} />
        {this.renderList()}
      </View>
    );
  }

  renderList() {
    const {user} = this.props;
    let listView = [];
    user.map((item, index) => {
      listView.push(
        <View key={index}>
          <Text>{item.name}</Text>
        </View>,
      );
    });
    return listView;
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(My);
