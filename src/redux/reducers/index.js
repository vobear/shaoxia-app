/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-26 16:26:14
 * @LastEditors: daxiong
 * @LastEditTime: 2021-02-05 13:07:45
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\reducers\index.js
 */
import { combineReducers } from 'redux';
import { CHANGE_NAME, update_chatList, update_like, update_chatListState } from '../action/actionType';
import updateUserInfo from './UserInfoReduer';
import loginStatus from './LoginReduer';

// 原始默认state
const defaultState = [
  {
    id: '001',
    name: '路人甲',
    sex: '女',
    age: 27,
    likeNumber: 0
  },
  {
    id: '002',
    name: '路人乙',
    sex: '女',
    age: 31,
    likeNumber: 0
  },
  {
    id: '003',
    name: '路人丙',
    sex: '男',
    age: 45,
    likeNumber: 0
  },
];

const defaultChatList = [];
const defaultChatList2 = [];

function user(state = defaultState, action) {
  switch (action.type) {
    case CHANGE_NAME:
      return state.map((item) =>
        item.id === action.id ? { ...item, name: action.name } : item,
      );
    default:
      return state;
  }
}

function updateChatList(state = defaultChatList, action) {
  // console.log('state=>', state);
  // console.log('action=>', action);
  switch (action.type) {
    case update_chatList:
      return (state = action.list);
    case update_like:
      return () => {
        console.log('[update_chatList state]', state)

        return ''
      }
    default:
      return state;
  }
}

function updateChatListState(state = defaultChatList2, action) {
  switch (action.type) {
    case update_chatListState:
      console.log('[updateChatListState state]', state)
      console.log('[updateChatListState action]', action)
      switch (action.state.command) {
        case 'updateList':
          console.log('[updateChatListState 更新]')
          return (state = action.state.list);
        case 'addLike':
          state = state.map(todo => todo.id === action.state.item.id ? { ...todo, likeNumber: todo.likeNumber + 1 } : todo)

          return state;
        case 'redLike':
          state = state.map(todo => todo.id === action.state.item.id ? { ...todo, likeNumber: todo.likeNumber - 1 } : todo)
          return state;
        case "pushItem":
          state = [...state, ...[action.state.item]]
          return (state)
      }

    default:
      return state;
  }
}

function updateLike(state = defaultChatList, action) {
  console.log('[updateLike]')
  switch (action.type) {
    case update_like:
      return () => {
        console.log('[update_chatList state]', action)
        // for (let i = 0; i < state.length; i++) {
        //   if (state[i].id == item.id) {
        //     state[i].likeNumber++;
        //     break;
        //   }
        // }
        return ''
      }
    default:
      return state;
  }
}



export default combineReducers({
  user,
  updateChatList,
  updateLike,
  loginStatus,
  updateUserInfo,
  updateChatListState
});
