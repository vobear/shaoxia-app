import {
  CHANGE_NAME,
  update_chatList,
  update_chatListState,
  update_like,
  login_status,
  update_userInfo
} from './actionType.js';

const changeName = (id, name) => ({ type: CHANGE_NAME, id: id, name: name });

const updateChatList = (list) => ({ type: update_chatList, list: list });

const updateChatListState = (state) => ({ type: update_chatListState, state: state });

const updateLike = (item) => ({ type: update_like, item: item });

const updateLoginStatus = (status) => ({ type: login_status, status: status });

const updateUserInfo = (userInfo) => ({
  type: update_userInfo,
  userInfo: userInfo,
});

// const updateLikeState = (state) => ({ type: updateLike, state: state })


export { changeName, updateChatList, updateChatListState,updateLike, updateLoginStatus, updateUserInfo };
