/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-10-26 16:23:58
 * @LastEditors: daxiong
 * @LastEditTime: 2021-02-05 10:37:36
 * @FilePath: e:\rn-dev\react-native-myapp\src\redux\action\actionType.js
 */
export const CHANGE_NAME = 'CHANGE_NAME';

export const update_chatList = 'update_chatList';
export const update_chatListState = 'update_chatListState';

export const update_like = 'update_like';

export const login_status = 'login_status';

export const update_userInfo = 'update_userInfo';

export const updateLike = 'updateLike';
