/*
 * @Descripttion: 路由配置文件
 * @Author: daxiong
 * @Date: 2020-09-24 11:40:59
 * @LastEditors: daxiong
 * @LastEditTime: 2021-11-23 16:45:21
 * @FilePath: e:\rn-dev\react-native-myapp\src\router\Router.js
 */

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {connect} from 'react-redux';

import Tabbar from '../router/Tabbar';
import Login from '../screen/Login/Login';
import Home from '../screen/Home/Home';
import Home1 from '../screen/Home/Home1';
import Home2 from '../screen/Home/Home2';
import Home3 from '../screen/Home/Home3';
import SectionWaterfall from '../screen/Home/SectionWaterfall';
import My from '../screen/My/My';
import My1 from '../screen/My/My1';
import My2 from '../screen/My/My2';
import My3 from '../screen/My/My3';

const AppNavigator = createStackNavigator(
  {
    Tabbar: {
      screen: Tabbar,
      navigationOptions: ({navigation, screenProps}) => ({
        headerShown: true,
      }),
    },
    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: true,
      },
    },
    Home: {
      screen: Home,
      navigationOptions: {
        headerShown: true,
      },
    },
    Home1: {
      screen: Home1,
      navigationOptions: {
        headerShown: false,
      },
    },
    Home2: {
      screen: Home2,
      navigationOptions: {
        headerShown: true,
      },
    },
    Home3: {
      screen: Home3,
      navigationOptions: {
        headerShown: true,
      },
    },
    SectionWaterfall: {
      screen: SectionWaterfall,
      navigationOptions: {
        headerShown: true,
      },
    },
    
    My: {
      screen: My,
      navigationOptions: {
        headerShown: true,
      },
    },
    My1: {
      screen: My1,
      navigationOptions: {
        headerShown: true,
      },
    },
    My2: {
      screen: My2,
      navigationOptions: {
        headerShown: true,
      },
    },
    My3: {
      screen: My3,
      navigationOptions: {
        headerShown: true,
      },
    },
  },
  {
    initialRouteName: 'Login',
    headerMode: 'screen',  //screen;none;float
    // mode: 'card',
    defaultNavigationOptions: ({navigation, screenProps}) => ({
      headerStyle: {
        backgroundColor: screenProps.themeColor,
      },
      // gestureEnabled: false, //是否可以使用手势关闭此屏幕。iOS-默认为true；Android-false。
      //当过渡动画开始时（在出现和隐藏屏幕时）都会调用的回调。
      onTransitionStart: (res) => {
        // console.log('[navigation 开始]=>', res);
        
      },
      //过渡动画结束时调用的回调
      onTransitionEnd: (res) => {
        // console.log('[navigation 结束]=>', res);
      },
    }),
  },
);


// const defaultGetStateForAction = AppNavigator.router.getStateForAction;
 
// AppNavigator.router.getStateForAction = (action, state) => {
//   // console.log('getStateForAction action=>',action)
//   // console.log('getStateForAction state=>',state)
//     //页面是MeScreen并且 global.user.loginState = false || ''（未登录）
//     // if (action.routeName ==='MeScreen'&& !global.user.loginState) {
//     //     this.routes = [
//     //         ...state.routes,
//     //         {key: 'id-'+Date.now(), routeName: 'Login', params: { name: 'name1'}},
//     //     ];
//     //     return {
//     //         ...state,
//     //         routes,
//     //         index: this.routes.length - 1,
//     //     };
//     // }
//     return defaultGetStateForAction(action, state);
// };


const mapStateToProps = (state) => ({
  loginStatus: state.loginStatus,
});

const mapDispatchToProps = (state) => ({
  loginStatus: state.loginStatus,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(createAppContainer(AppNavigator));
