/*
 * @Descripttion: 文件描述
 * @Author: daxiong
 * @Date: 2020-09-24 18:46:18
 * @LastEditors: daxiong
 * @LastEditTime: 2020-10-22 11:12:24
 * @FilePath: e:\rn-dev\react-native-myapp\src\router\Tabbar.js
 */
import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import My from '../screen/My/My';
import Home from '../screen/Home/Home';

class TabsItem extends React.Component {
  render() {
    const {title, focused} = this.props;
    let txtActive = [{color: '#999'}];
    if (focused) {
      txtActive = [{color: '#333'}];
    }
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={[txtActive]}>{title}</Text>
      </View>
    );
  }
}

export default BottomNavigator = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: ({navigation}) => ({
      tabBarPosition: 'bottom',
      tabBarLabel: ({focused}) => {
        return <TabsItem title={'首页'} focused={focused} />;
      },
      tabBarIcon: ({tintColor, focused}) => (
        <Image
          style={styles.tabIcon}
          source={
            focused
              ? require('../image/tab/tab1_1.png')
              : require('../image/tab/tab1_0.png')
          }
        />
      ),
      tabBarOnPress: () => {
        route(navigation);
      },
    }),
  },
  My: {
    screen: My,
    navigationOptions: ({navigation}) => ({
      tabBarPosition: 'bottom',
      tabBarLabel: ({focused}) => {
        return <TabsItem title={'我的'} focused={focused} />;
      },
      tabBarIcon: ({tintColor, focused}) => (
        <Image
          style={styles.tabIcon}
          source={
            focused
              ? require('../image/tab/tab2_1.png')
              : require('../image/tab/tab2_0.png')
          }
        />
      ),
      tabBarOnPress: () => {
        route(navigation);
      },
    }),
  },
});

const route = (navigation) => {
  console.log('navigation=>', navigation);
  if (!navigation.isFocused()) {
    navigation.navigate(navigation.state.routeName, {
      title: navigation.state.routeName,
    });
  }
};

const styles = StyleSheet.create({
  tabIcon: {
    width: 26,
    height: 26,
  },
});
